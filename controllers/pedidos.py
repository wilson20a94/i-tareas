from handler import Handler
from models.pedido import Pedido

class PedidosHandler(Handler):
    def get(self):
        pedidos = Pedido.all()
        if not pedidos:
            self.write("No hay pedidos")
            return
        pedidos = list(pedidos)
        self.render('resultPedidos.html', pedidos=pedidos)