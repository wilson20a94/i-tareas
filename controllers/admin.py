from handler import Handler

class AdminHandler(Handler):
    def get(self):
    	if not self.user:
            self.redirect("/login")
        else:
            self.render('dashboard.html')