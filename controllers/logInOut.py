from handler import Handler
from models.user import User

class LogInHandler(Handler):
    def get(self):
        if self.user:        
            self.redirect("/adm")
        else:
            self.render("LogIn.html")

    def post(self):
        username = self.request.get("username")
        password = self.request.get("password")
        u = User.login(username, password)
        if u:
            self.login(u)
            self.redirect('/adm')
        else:
            self.render("LogIn.html", error="Please check your username and password.")
            
class LogOutHandler(Handler):
    def get(self):
        self.logout()
        self.redirect("/login")