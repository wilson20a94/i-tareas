from handler import Handler
from models.user import User
from utilities import userHandling

class SignUpHandler(Handler):
    def render_front(self, username="", email="", user_error="", exists_error="", password_error="", verify_error="", email_error=""):
        self.render("SignUp.html", user=username, email=email, usererror=user_error, passworderror=password_error, verifyerror=verify_error, emailerror=email_error)

    def get(self):
        if self.user:
            self.redirect("/adm")
        self.render_front()

    def post(self):
        self.username = self.request.get('username')
        self.password = self.request.get('password')
        self.verify = self.request.get('verify')
        self.email = self.request.get('email')

        exists = False
        u = User.by_name(self.username.lower())
        if u:
            exists = True

        user = userHandling.valid_username(self.username)
        password = userHandling.valid_password(self.password)
        verify = userHandling.valid_verify(self.password, self.verify)
        email = userHandling.valid_email(self.email)

        user_message = "That's not a valid username."
        user_message_exists = "That user already exists"
        password_message = "That's not a valid password."
        verify_message = "Your passwords didn't match."
        email_message = "That's not a valid email."

        if user and not exists:
            user_message = ""

        elif user and exists:
            user_message = "That user already exists"

        if password:
            password_message = ""
        if verify:
            verify_message = ""
        if email:
            email_message = ""

        if user and password and verify and email and not exists: #if all True
            u = User.register(self.username, self.password, self.email)
            u.put()

            self.login(u)
            self.redirect("/adm")
        else:
            self.render_front(self.username, self.email, user_message, user_message_exists, password_message, verify_message, email_message)

