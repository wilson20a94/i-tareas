from handler import Handler
from models.pedido import Pedido



def cambiarEstado(keyPedido, nEstado):
    pedido = Pedido.get_by_id(int(keyPedido))
    pedido.estado = nEstado
    pedido.put()


class PedidoHandler(Handler):
    def get(self, estado, reqId):
        pedido = Pedido.get_by_id(int(reqId))
        if pedido and pedido.estado == estado:
            self.render('pedido.html', pedido=pedido)
            # self.write(pedido)
        else:
            self.redirect('/adm/pedidos')

    def post(self, estado, reqId):
        key = str(self.request.get("key_pedido"))
        reqCancelar = str(self.request.get("Cancelar"))
        reqPendiente = str(self.request.get("Pendiente"))
        reqCompletar = str(self.request.get("Completar"))

        if key == reqId:
            if reqPendiente:
                nEstado = "Pendiente"

            elif reqCancelar:
                nEstado = "Cancelado"

            elif reqCompletar:
                nEstado = "Completado"

            cambiarEstado(key, nEstado)

            self.redirect("/adm/pedidos")
        else:
            self.wrte('Las keys no coinciden')


