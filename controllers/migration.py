from handler import Handler
from models.user import User

class MigrationHandler(Handler):
    def get(self):
    	users = User.all()
        for i in users:
            i.delete()

        u = User.register('admin', 'Passw0rd')
        u.put()

        self.redirect('/')