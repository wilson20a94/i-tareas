from handler import Handler
from models.pedido import Pedido
import datetime

class ProyectaHandler(Handler):
    def get(self):
        self.render("iproyecta.html")

    def post(self):
        nombre = str(self.request.get("nombre"))
        fecha = datetime.datetime.strptime(self.request.get('fecha'), "%Y-%m-%dT%H:%M")
        aula_edificio = str(self.request.get("aula_edificio"))
        observaciones = str(self.request.get("observaciones"))
        especifique = str(self.request.get("especifique"))
        celular = str(self.request.get("celular"))
        lista_servicios = self.request.get("servicio", allow_multiple=True)

        total=0
        # Codigo provisional para agilizar proceso de deploy 18/3/2016
        for servicio in lista_servicios:
            if str(servicio) == 'Bocinas':
                total+=25
            if str(servicio) == 'Laptop':
                total+=50
            if str(servicio) == 'Puntero':
                total+=25
            if str(servicio) == 'DataShow':
                total+=50
        if especifique == "llevar":
            total+=25

        
        fecha_utc = fecha + datetime.timedelta(hours=4) #(UTC-04:00) Georgetown, La Paz, Manaos, San Juan. No tiene cambio de horario, basta con sumar la diferencia de horas.
        current_time_plus_hour = datetime.datetime.now() + datetime.timedelta(hours=1)

        if fecha_utc >= current_time_plus_hour: 
            a = Pedido(
                nombre = nombre,
                fecha = fecha.__str__(),
                aula_edificio = aula_edificio,
                observaciones = observaciones,
                servicios = lista_servicios,
                especifique = especifique,
                celular = celular,
                total=total,
                estado="Pendiente")
            a.put()
            self.render("iproyecta.html", success='Su solicitud ha sido enviada.')
        else:
            errorlog = "Inserte una fecha y hora valida. (Solo se permite hacer solicitudes con una hora de anticipaci"+u"\u00F3"+"n)" 
            self.render("iproyecta.html", nombre=nombre, aula_edificio=aula_edificio, observaciones=observaciones, celular=celular, error= errorlog)
