from handler import Handler
from models.productos import Productos


class ProductosHandler(Handler):
    def get(self):
        productos = Productos.all()
        self.render('productos.html', productos=productos)

    def post(self):
        nombre = str(self.request.get("nombre"))
        precio = int(self.request.get("precio"))
        estado = str(self.request.get("estado"))
        typeProd = str(self.request.get("typeProd"))
        nombreDeServicioPertenece = str(self.request.get("nombreDeServicioPertenece"))
        prod = Productos(
                nombre = nombre,
                precio = precio,
                estado = estado,
                typeProd = typeProd,
                nombreDeServicioPertenece = nombreDeServicioPertenece)
        prod.put()
        productos = Productos.all()
        self.render('productos.html', productos=productos)
