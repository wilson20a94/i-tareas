var refreshTotal = function(){
	var total = 0;
	$("form input:checkbox:checked").parent().parent().find("span").not(".pricing").each(function(index, x, y) {
		total += parseInt($(this).text());
	});

	$("form input:radio:checked").parent().parent().find("span").not(".pricing").each(function(index, x, y) {
		total += parseInt($(this).text());
	});

	$("#total").text("RD$" + total);
}

$("form input:checkbox").change(function() {
	refreshTotal();
});

$("form input:radio").change(function(){
	refreshTotal();
});