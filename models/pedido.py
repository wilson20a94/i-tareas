from google.appengine.ext import db

class Pedido(db.Model):
    nombre = db.StringProperty(required = True)
    fecha = db.StringProperty(required = True)
    aula_edificio = db.StringProperty(required = True)
    observaciones = db.TextProperty()
    servicios = db.StringListProperty()
    especifique = db.StringProperty(required = True)
    celular = db.StringProperty(required = True)
    total = db.IntegerProperty(required = True)
    estado = db.StringProperty(required = True)

# IntegerProperty int long 
# FloatProperty float   Numeric
# BooleanProperty bool    False < True
# StringProperty  str 
# TextProperty    db.Text None
# DateProperty 
# TimeProperty 
# DateTimeProperty    datetime.date 
# PhoneNumberProperty db.PhoneNumber  Unicode
# EmailProperty   db.Email    Unicode
# RatingProperty  db.Rating   Numeric
# ReferenceProperty 
# SelfReferenceProperty   db.Key  By path elements 
# ListProperty 
# StringListProperty  list of a supported type    If ascending, by least element; 
# https://cloud.google.com/appengine/docs/python/datastore/typesandpropertyclasses