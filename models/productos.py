from google.appengine.ext import db

class Productos(db.Model):
    nombre = db.StringProperty(required = True)
    precio = db.IntegerProperty(required = True)
    estado = db.StringProperty(required = True)
    typeProd = db.StringProperty(required = True)
    nombreDeServicioPertenece = db.StringProperty(required = True) # Iinvestiga, iproyecta, etc

# IntegerProperty int long
# FloatProperty float   Numeric
# BooleanProperty bool    False < True
# StringProperty  str
# TextProperty    db.Text None
# DateProperty
# TimeProperty
# DateTimeProperty    datetime.date
# PhoneNumberProperty db.PhoneNumber  Unicode
# EmailProperty   db.Email    Unicode
# RatingProperty  db.Rating   Numeric
# ReferenceProperty
# SelfReferenceProperty   db.Key  By path elements
# ListProperty
# StringListProperty  list of a supported type    If ascending, by least element;
# https://cloud.google.com/appengine/docs/python/datastore/typesandpropertyclasses
