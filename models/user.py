from google.appengine.ext import db
from utilities import userHandling

class User(db.Model):
    username = db.StringProperty(required = True)
    profile_name = db.StringProperty(required = True)
    password = db.StringProperty(required = True)
    email = db.StringProperty()
    fecha_creacion = db.DateTimeProperty(auto_now_add = True)

    @classmethod
    def by_id(cls, uid):
        return User.get_by_id(uid)

    @classmethod
    def by_name(cls, username):
        u = User.all().filter('username =', username).get()
        return u

    @classmethod
    def register(cls, username, pw, email = None):
        password = userHandling.make_pw_hash(username, pw)
        return User(username = username.lower(),
                    profile_name = username,
                    password = password,
                    email = email)

    @classmethod
    def login(cls, username, pw):
        u = cls.by_name(username)
        if u and userHandling.valid_pw(username, pw, u.password):
            return u