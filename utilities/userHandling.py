import re
import random
import string
import hashlib
import hmac

USER_UN = re.compile(r"^[a-zA-Z0-9._-]{4,20}$")
USER_PW = re.compile(r"^.{3,20}$")
USER_EM = re.compile(r"^[\S]+@[\S]+\.[\S]+$")
secret = "fart"

def valid_username(username):
    if USER_UN.match(username) is not None:
        return True
    return False

def valid_exists(username):
    return getByName(username)

def valid_password(password):
    if USER_PW.match(password) is not None:
        return True
    return False

def valid_email(email):
    if email == "":
        return True
    if USER_EM.match(email) is not None:
        return True
    return False

def valid_verify(password, verify):
    if password == verify:
        return True
    return False

def make_salt():
    # returns a string of 5 random letters use python's random module.
    return ''.join(random.choice(string.letters) for x in xrange(5))

def make_pw_hash(name, pw, salt = None):
    # returns a hashed password of the format: (name + pw + salt),salt
    if not salt:
        salt = make_salt()
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s|%s' % (h, salt)

def valid_pw(name, pw, h):
    # returns True if a user's password matches its hash.
    salt = h.split('|')[1]
    return h == make_pw_hash(name, pw, salt)

def make_secure_val(val):
    return '%s|%s' % (val, hmac.new(secret, val).hexdigest())

def check_secure_val(secure_val):
    val = secure_val.split('|')[0]
    if secure_val == make_secure_val(val):
        return val