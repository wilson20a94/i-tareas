import webapp2

from controllers.iproyecta import ProyectaHandler
from controllers.front import MainHandler
from controllers.admin import AdminHandler
from controllers.pedidos import PedidosHandler
from controllers.pedido import PedidoHandler
from controllers.migration import MigrationHandler
from controllers.signup import SignUpHandler
from controllers.productos import ProductosHandler
import controllers.logInOut


# import json
# import urllib2
# import re
# import hashlib
# import random
# import string
# import logging
# import time
# import hmac

# from google.appengine.api import memcache

# CAMBIAR


ROUTES = [
        ('/', MainHandler),
        # ('/signup', SignUpHandler),
        ('/login', controllers.logInOut.LogInHandler),
        ('/logout', controllers.logInOut.LogOutHandler),
        ('/adm', AdminHandler),
        ('/adm/pedidos', PedidosHandler),
        (r'/adm/pedidos/([a-zA-Z]+)/(.+)', PedidoHandler),
        ('/adm/pedidos', PedidosHandler),
        ('/iproyecta', ProyectaHandler), 
        ('/migration', MigrationHandler),
        ('/adm/productos', ProductosHandler), # <-- Siempre termina en ","
        ]

app = webapp2.WSGIApplication(ROUTES, debug=True)
